package com.example.shopmanagement;

import com.example.shopmanagement.R;
import com.example.shopmanagement.R.id;
import com.example.shopmanagement.R.layout;
import com.example.shopmanagement.R.menu;

import android.app.Activity;
import android.app.ActionBar;
import android.app.Fragment;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.os.Build;

public class VIPInformation extends  Activity implements View.OnClickListener {
    public VIP_database user;
    public  SQLiteDatabase sqL_read;
    private Button search_del_btn,insert_btn,update_btn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vipinformation);
        VIP_database user=new VIP_database(VIPInformation.this);
        //获取一个可读的数据库
        sqL_read=user.getReadableDatabase();
        init();
    }
    
    //组件初始化方法
    public void init(){
        search_del_btn=(Button)findViewById(R.id.search_deleteBtn);
        insert_btn=(Button)findViewById(R.id.addBtn);
        update_btn=(Button)findViewById(R.id.updateBtn);
        //添加监听
        search_del_btn.setOnClickListener(this);
        insert_btn.setOnClickListener(this);
        update_btn.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.search_deleteBtn:
                Intent intent1=new Intent(VIPInformation.this,SearchDelVIP.class);
                startActivity(intent1);
                break;
            case R.id.addBtn:
                Intent intent2=new Intent(VIPInformation.this,InsertVIP.class);
                startActivity(intent2);
                break;
            case R.id.updateBtn:
                Intent intent3=new Intent(VIPInformation.this,UpdateVIP.class);
                startActivity(intent3);
                break;
            default:
                break;
        }
    }
}
