package com.example.shopmanagement;

import com.example.shopmanagement.R;
import com.example.shopmanagement.R.id;
import com.example.shopmanagement.R.layout;
import com.example.shopmanagement.R.menu;

import android.app.Activity;
import android.app.ActionBar;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.os.Build;

public class VIPInfo {
    private int id;
    private String username;
    private String pswd;
    private String sex;
    private int age;

    public VIPInfo(int id,String username,String paswd,String sex,int age) {
        this.id=id;
        this.username = username;
        this.pswd = paswd;
        this.sex = sex;
        this.age = age;
    }
    public void setId(int id) {
        this.id = id;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPaswd(String paswd) {
        this.pswd = paswd;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getPaswd() {
        return pswd;
    }

    public String getSex() {
        return sex;
    }

    public int getAge() {
        return age;
    }
    @Override
    public String toString() {
        return "userInfo{" + "id=" + id + ", username='" + username + '\'' +
                ", paswd='" + pswd + '\'' + ", sex='" + sex + '\'' +
                ", age=" + age + '}';
    }
}

