package com.example.shopmanagement;

import android.app.Activity;
import android.app.ActionBar;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.os.Build;

public class ShoesDetails extends Activity {
	Button sneakersBtn;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_shoes_details);
		sneakersBtn=(Button) this.findViewById(R.id.sneakersBtn);
		sneakersBtn.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View arg0) {
				// 步骤1：添加一个FragmentTransaction的实例
				FragmentManager fragmentManager = getFragmentManager();
				FragmentTransaction transaction = fragmentManager.beginTransaction();
				// 步骤2：用add()方法加上Fragment的对象rightFragment
				Right5 right5 = new Right5();
				transaction.add(R.id.right, right5);
				// 步骤3：调用commit()方法使得FragmentTransaction实例的改变生效
				transaction.commit();
			}
		});

	}

}
