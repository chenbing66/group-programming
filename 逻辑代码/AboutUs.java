package com.example.shopmanagement;

import android.app.Activity;
import android.app.ActionBar;
import android.app.Fragment;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;
import android.widget.TabWidget;
import android.widget.TextView;
import android.widget.Toast;
import android.os.Build;

public class AboutUs extends Activity {
	Button dialBtn;
	TextView numTv;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_about_us);
		dialBtn = (Button) this.findViewById(R.id.dialBtn);
		numTv = (TextView) this.findViewById(R.id.numTv);

		TabHost tabhost = (TabHost) this.findViewById(R.id.tabhost);
		tabhost.setup();
		TabSpec page1 = tabhost.newTabSpec("tab1").setIndicator("关于我们")
				.setContent(R.id.content1);
		tabhost.addTab(page1);
		TabSpec page2 = tabhost.newTabSpec("tab2").setIndicator("联系我们")
				.setContent(R.id.content2);
		tabhost.addTab(page2);

		// 设置选项卡的高度和宽度
		TabWidget mTabWidget = tabhost.getTabWidget();
		for (int i = 0; i < mTabWidget.getChildCount(); i++) {
			// 设置选项卡的高度
			mTabWidget.getChildAt(i).getLayoutParams().height = 80;
			// 设置选项卡的宽度
			mTabWidget.getChildAt(i).getLayoutParams().width = 60;
		}

		dialBtn.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				//取得输入的电话号码串
				String numStr = numTv.getText().toString();
				//创建打电话的Intent
					Intent phoneIntent = new Intent(
							"android.intent.action.CALL", Uri.parse("tel:"
									+ numStr));
					startActivity(phoneIntent);	
			}
		});
	}
}
