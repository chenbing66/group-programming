package com.example.shopmanagement;

import android.app.Activity;
import android.app.ActionBar;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.os.Build;

public class Storage extends Activity {
	Button clothBtn,trousersBtn,shoesBtn;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_storage);

		clothBtn=(Button) this.findViewById(R.id.clothBtn);
		trousersBtn=(Button) this.findViewById(R.id.trousersBtn);
		shoesBtn=(Button) this.findViewById(R.id.shoesBtn);
		clothBtn.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View arg0) {
				Intent intent=new Intent(Storage.this,ClothDetails.class);
				startActivity(intent);
			}
		});
		trousersBtn.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View arg0) {
				Intent intent=new Intent(Storage.this,TrousersDetails.class);
				startActivity(intent);
			}
		});
		shoesBtn.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View arg0) {
				Intent intent=new Intent(Storage.this,ShoesDetails.class);
				startActivity(intent);
			}
		});
	}
}
