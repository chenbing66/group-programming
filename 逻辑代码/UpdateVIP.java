package com.example.shopmanagement;

import com.example.shopmanagement.R;
import com.example.shopmanagement.R.layout;

import android.app.Activity;
import android.app.ActionBar;
import android.app.Fragment;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;
import android.os.Build;

public class UpdateVIP extends Activity implements OnClickListener{
	 private EditText nameEdt,pswdEdt,ageEdt;
	 private Spinner spinner;
	 private String select_sex;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_update_vip);
		
		nameEdt=(EditText)findViewById(R.id.nameEdt);
		pswdEdt=(EditText)findViewById(R.id.pswdEdt);
		ageEdt=(EditText)this.findViewById(R.id.ageEdt);
       spinner=(Spinner) findViewById(R.id.spinnerxingbie);
       //为选择性别下拉列表添加选择事件
       spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
           @Override
           public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
               //获取选择的值
               select_sex=UpdateVIP.this.getResources().getStringArray(R.array.sex)[i];
           }

           @Override
           public void onNothingSelected(AdapterView<?> adapterView) {

           }
       });
       
		Button bt1=(Button)this.findViewById(R.id.sureBtn);
		bt1.setOnClickListener(this);
	}

	@Override
	public void onClick(View arg0) {
		String name_str=nameEdt.getText().toString();
        // Toast.makeText(InsertUser_Activity.this,name_str,Toast.LENGTH_SHORT).show();
        String paswd_str=pswdEdt.getText().toString();
        int age=Integer.parseInt(ageEdt.getText().toString());
        //调用数据库操作类的插入方法
        VIP_database us_db=new VIP_database(UpdateVIP.this);
        SQLiteDatabase sqLiteDatabase=us_db.getWritableDatabase();
        us_db.update(sqLiteDatabase,name_str,paswd_str,select_sex,age);
        
        Toast.makeText(UpdateVIP.this,"更新成功",Toast.LENGTH_SHORT).show();
	}
}

