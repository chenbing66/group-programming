package com.example.shopmanagement;

import android.app.Activity;
import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.*;
import android.os.Build;

public class MainActivity extends Activity {
	Button register,login;
	EditText nameEdt,pswdEdt;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        register=(Button) this.findViewById(R.id.rgstBtn);
		login=(Button) this.findViewById(R.id.loninBtn);
		nameEdt=(EditText) this.findViewById(R.id.nameEdt);
		pswdEdt=(EditText) this.findViewById(R.id.pwdEdt);
		
		//用匿名内部类为注册按钮添加事件监听器
		register.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				Intent intent=new Intent();
				intent.setClass(MainActivity.this, RegisterActivity.class);
				startActivity(intent);
			}
		});
		
		final String name="admin";
		final String pswd="1234aaa";
		//用匿名内部类为登录按钮添加事件监听器
		login.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View arg0) {
				String name1="";
				String pswd1="";
				name1=nameEdt.getText().toString();
				pswd1=pswdEdt.getText().toString();
				if(name1.equals(name)&&pswd1.equals(pswd)){
					Intent intent=new Intent(MainActivity.this,SelectionOne.class);
					startActivity(intent);
				}
				else{
					new AlertDialog.Builder(MainActivity.this).setTitle("Error!").
					setMessage("用户名或密码错误") 
				    .setNegativeButton("OK",null) 
				    .show(); 
				}
			}
		});
    }
}
