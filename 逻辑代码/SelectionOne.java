package com.example.shopmanagement;

import com.example.shopmanagement.R.id;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;

public class SelectionOne extends Activity{
	Button vipInfoBtn,contactsBtn,mallBtn;
			
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_selection_one);
		vipInfoBtn=(Button) this.findViewById(R.id.VipInfoBtn);
		contactsBtn=(Button) this.findViewById(R.id.contactsBtn);
		mallBtn=(Button) this.findViewById(R.id.mallBtn);
		vipInfoBtn.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View arg0) {
				Intent intent=new Intent(SelectionOne.this,VIPInformation.class);
				startActivity(intent);
			}
		});
		contactsBtn.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View arg0) {
				Intent intent=new Intent(SelectionOne.this,StuffContacts.class);
				startActivity(intent);
			}
		});
		mallBtn.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View arg0) {
				Intent intent=new Intent(SelectionOne.this,Storage.class);
				startActivity(intent);
			}
		});
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater=getMenuInflater();
		inflater.inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()){
		case id.aboutUs:
			Intent intent=new Intent(SelectionOne.this,AboutUs.class);
			startActivity(intent);
			break;
		case id.exit:
			android.content.DialogInterface.OnClickListener listener=new 
			android.content.DialogInterface.OnClickListener(){
				@Override
				public void onClick(DialogInterface dialog, int which) {
					System.exit(0); 
				}	
			};
			Builder builder=new Builder(SelectionOne.this);
			builder.setTitle("确定要退出吗？");
			builder.setPositiveButton("确定",listener);
			builder.setNegativeButton("取消",null);
			builder.show();
			break;
		}
		return true;
    }
}
