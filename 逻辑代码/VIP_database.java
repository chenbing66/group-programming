package com.example.shopmanagement;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class VIP_database extends SQLiteOpenHelper {
    public VIP_database(Context context) {
        super(context, "user_db",null,1);
    }
   
    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String sql="create table user(id integer primary key autoincrement,username varchar(20),"
        		+ "paswd varchar(20),sex varchar(20),age integer)";
        sqLiteDatabase.execSQL(sql);
    }
   
    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
    }
    
    public void adddata(SQLiteDatabase sqLiteDatabase,String username,String paswd,String sex,int age){
        ContentValues values=new ContentValues();
        values.put("username",username);
        values.put("paswd",paswd);
        values.put("sex",sex);
        values.put("age",age);
        sqLiteDatabase.insert("user",null,values);
        sqLiteDatabase.close();
    }
   
    public void delete(SQLiteDatabase sqLiteDatabase,int id){
        sqLiteDatabase.delete("user","id=?", new String[]{id+""});
        sqLiteDatabase.close();
    }
    
    public void update(SQLiteDatabase sqLiteDatabase,String username,String paswd,String sex,int age){
        ContentValues values=new ContentValues();
        values.put("username",username);
        values.put("paswd",paswd);
        values.put("sex",sex);
        values.put("age",age);
        sqLiteDatabase.update("user",values,"username=?",new String[]{username});
        sqLiteDatabase.close();
    }
    
    public List<VIPInfo> querydata(SQLiteDatabase sqLiteDatabase){
        Cursor cursor=sqLiteDatabase.query("user",null,null,null,null,null,"id ASC");
        List<VIPInfo> list=new ArrayList<VIPInfo>();
        while(cursor.moveToNext()){
            int id=cursor.getInt(cursor.getColumnIndex("id"));
            String username=cursor.getString(1);
            String paswd=cursor.getString(2);
            String sex=cursor.getString(3);
            int age=cursor.getInt(cursor.getColumnIndex("age"));
            list.add(new VIPInfo(id,username,paswd,sex,age));
        }
        cursor.close();
        sqLiteDatabase.close();
        return list;
    }
}
