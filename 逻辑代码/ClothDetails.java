package com.example.shopmanagement;

import android.app.Activity;
import android.app.ActionBar;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ListActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.os.Build;

public class ClothDetails extends Activity implements OnClickListener {
	Button TBtn,shirtBtn;
	        
	@Override
	protected void onCreate( Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_cloth_details);
		TBtn=(Button) this.findViewById(R.id.TBtn);
		shirtBtn=(Button) this.findViewById(R.id.shirtBtn);
		TBtn.setOnClickListener(this);
		shirtBtn.setOnClickListener(this);
		
	}
		
	public void onClick(View v){
		switch(v.getId()){
		case R.id.TBtn:
			// 步骤1：添加一个FragmentTransaction的实例
			FragmentManager fragmentManager = getFragmentManager();
			FragmentTransaction transaction = fragmentManager.beginTransaction();
			// 步骤2：用add()方法加上Fragment的对象rightFragment
			Right1 right1 = new Right1();
			transaction.add(R.id.right, right1);
			// 步骤3：调用commit()方法使得FragmentTransaction实例的改变生效
			transaction.commit();
			break;
		case R.id.shirtBtn:
			// 步骤1：添加一个FragmentTransaction的实例
			FragmentManager fragmentManager1 = getFragmentManager();
			FragmentTransaction transaction1 = fragmentManager1.beginTransaction();
			// 步骤2：用add()方法加上Fragment的对象rightFragment
			Right2 right2 = new Right2();
			transaction1.add(R.id.right, right2);
			// 步骤3：调用commit()方法使得FragmentTransaction实例的改变生效
			transaction1.commit();
			break;
		}
	}
}
