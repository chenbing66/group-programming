package com.example.shopmanagement;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import android.app.Activity;
import android.app.ActionBar;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.*;
import android.os.Build;

public class RegisterActivity extends Activity implements View.OnClickListener,
		RadioGroup.OnCheckedChangeListener{
	private String phone_str = "";
	private String pswd_str = "";
	private String name_str = "";
	private String sex_str = "男性";
	private String city_str = "";
	private String date_str = "";
	EditText phone_edit, pswd_edit, name_edit,date_edit;
	RadioGroup sex_group;
	RadioButton male_rdbtn, female_rdbtn;
	Button register, chooseDate;
	Spinner spinner;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_register);

		phone_edit = (EditText) findViewById(R.id.phone);
		pswd_edit = (EditText) findViewById(R.id.pswd);
		name_edit = (EditText) findViewById(R.id.name);
		date_edit = (EditText) findViewById(R.id.date);
		sex_group = (RadioGroup) findViewById(R.id.sex);
		male_rdbtn = (RadioButton) findViewById(R.id.male);
		register = (Button) findViewById(R.id.registerBtn);
		chooseDate = (Button) findViewById(R.id.dateBtn);
		spinner = (Spinner) findViewById(R.id.spinner);

		sex_group.setOnCheckedChangeListener(this);
		register.setOnClickListener(this);
		chooseDate.setOnClickListener(this);

		//用适配器为下拉列表添加选项
		final String[] city = new String[] { "北京", "上海", "武汉", "南京", "南昌" };
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, city);
		spinner.setAdapter(adapter);
		//为下拉列表添加单击选项的事件监听器
		spinner.setOnItemSelectedListener(new Spinner.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> adapterView, View view,
					int i, long l) {
				city_str = city[i];
			}
			@Override
			public void onNothingSelected(AdapterView<?> adapterView) {

			}
		});
	}

	//日期选择对话框的事件监听器
	final Calendar c=Calendar.getInstance();
	private DatePickerDialog.OnDateSetListener listener = new DatePickerDialog.OnDateSetListener(){
		@Override
    	public void onDateSet(DatePicker arg0, int arg1, int arg2, int arg3) {
		c.set(Calendar.YEAR,arg1);
		c.set(Calendar.MONTH,arg2);
		c.set(Calendar.DAY_OF_MONTH,arg3);
		updateDate();
		}
	};
	private void updateDate() {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		date_edit.setText(df.format(c.getTime()));
	}
	//注册、选择日期按钮的事件处理方法
	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.registerBtn:
			phone_str = phone_edit.getText().toString();
			pswd_str = pswd_edit.getText().toString();
			name_str = name_edit.getText().toString();
			date_str=date_edit.getText().toString();
			// 传递数据给下一个Activity
			Intent intent = new Intent(this,RegisterDone.class);
			Bundle bundle = new Bundle();
			bundle.putString("phone", phone_str);
			bundle.putString("paswd", pswd_str);
			bundle.putString("name", name_str);
			bundle.putString("date", date_str);
			bundle.putString("sex", sex_str);
			bundle.putString("city", city_str);
			intent.putExtras(bundle);
			startActivity(intent);
			break;
		case R.id.dateBtn:
			new DatePickerDialog(RegisterActivity.this ,listener,
					c.get(Calendar.YEAR),c.get(Calendar.MONTH),c.get(Calendar.DAY_OF_MONTH)).show();
		}
	}

	// 性别的事件处理方法
	@Override
	public void onCheckedChanged(RadioGroup radioGroup,int i) {
		// 根据用户选择来改变sex_str的值
		sex_str = i == R.id.female ? "男性" : "女性";
	}
}


	
