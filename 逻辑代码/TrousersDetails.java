package com.example.shopmanagement;

import android.app.Activity;
import android.app.ActionBar;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.os.Build;

public class TrousersDetails extends Activity {
	Button jeansBtn,shortsBtn;
    
	@Override
	protected void onCreate( Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_trousers_details);
		jeansBtn=(Button) this.findViewById(R.id.jeansBtn);
		shortsBtn=(Button) this.findViewById(R.id.shortsBtn);
		jeansBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// 步骤1：添加一个FragmentTransaction的实例
				FragmentManager fragmentManager = getFragmentManager();
				FragmentTransaction transaction = fragmentManager.beginTransaction();
				// 步骤2：用add()方法加上Fragment的对象rightFragment
				Right3 right3 = new Right3();
				transaction.add(R.id.right, right3);
				// 步骤3：调用commit()方法使得FragmentTransaction实例的改变生效
				transaction.commit();
			}
		});
		shortsBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// 步骤1：添加一个FragmentTransaction的实例
				FragmentManager fragmentManager = getFragmentManager();
				FragmentTransaction transaction = fragmentManager.beginTransaction();
				// 步骤2：用add()方法加上Fragment的对象rightFragment
				Right4 right4 = new Right4();
				transaction.add(R.id.right, right4);
				// 步骤3：调用commit()方法使得FragmentTransaction实例的改变生效
				transaction.commit();
			}
		});
	}
		
}
