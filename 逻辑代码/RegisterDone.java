package com.example.shopmanagement;

import android.app.Activity;
import android.app.ActionBar;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import android.os.Build;

public class RegisterDone extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_register_done);
		Intent intent=this.getIntent();
        Bundle bundle=intent.getExtras();
        String phone=bundle.getString("phone");
        String name=bundle.getString("name");
        String date=bundle.getString("date");
        String pswd=bundle.getString("pswd");
        String sex=bundle.getString("sex");
        String city=bundle.getString("city");
        TextView show_text=(TextView)findViewById(R.id.show_content);
        show_text.setText("手机号为："+phone+"\n"+"用户名为："+name+"\n"+"密码为："+pswd+"\n"+
        "出生日期为："+date+"\n"+"性别是："+sex+"\n"+"城市是："+city);
	}
}
